<?php
require_once './dbconnect/connection.php';

$statement = $pdo->prepare("SELECT * FROM TIPO");
$statement->execute();
$tipos = $statement->fetchAll(PDO::FETCH_ASSOC);

$nome = $_POST['name'] ?? null;

$statement = $pdo->prepare("SELECT ITEM.NOME, LOCALIZACAO, TIPO.NOME FROM ITEM, TIPO WHERE TIPO.ID = IDTIPO and TIPO.NOME= :nome ;");
$statement->bindValue(':nome',$nome);
$statement->execute();
$items = $statement->fetchAll(PDO::FETCH_ASSOC)
?>
<?php include_once "./partials/header.php"; ?>

<body>
    <?php include_once "./partials/navbar.php" ?>

    <div class="container">
        <h1 class="title">Tipos</h1>
        <a class="newNote" href="createtype.php">Criar novo</a>
        <div>
            <table class="notes-table">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Nome</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($tipos as $tipo) : ?>
                        <tr>
                            <td> <?php echo $tipo['ID']; ?> </td>
                            <td> <?php echo $tipo['NOME']; ?> </td>
                            <td class="buttons">
                                <form action="deletetype.php" method="POST">
                                    <input type="hidden" name="typeId" value="<?php echo $tipo['ID']?>">
                                    <button type ="submit" class="deleteButton" type="button">Apagar</button>
                                </form>
                            </td>
                        </tr>
                    <?php endforeach; ?>

                </tbody>
            </table>
        </div>
        <div>
            <h1 class="title"> Filtragem </h1>
            <div class="edit-form-background-inner">
                <span class="edit-form-title">Tipo de Apontamento</span>
                <form action="" method="POST">
                    <div class="boxes">
                        <label for="idtipo">Tipo</label>
                        <select name="idtipo">
                            <?php
                            $statement = $pdo->prepare("SELECT * FROM TIPO");
                            $statement->execute();
                            $tipos = $statement->fetchall(PDO::FETCH_ASSOC);

                            foreach ($tipos as $tipo) :
                                if ($item['IDTIPO'] == $tipo['ID']) { ?>
                                    <option value="<?php echo $tipo['ID'] ?>" selected><?php echo $tipo['NOME']; ?></option>
                                    <?php
                                } else {
                                    ?>
                                    <option value="<?php echo $tipo['ID'] ?>"><?php echo $tipo['NOME']; ?></option>
                                    <?php
                                }
                                ?>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="boxes">
                        <input type="submit" name="submit" value="Guardar">
                    </div>
                </form>
                <div>
                    <table class="notes-table">
                        <thead>
                        <tr>
                            <th>Tipo</th>
                            <th>Descrição</th>
                            <th>Localização</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                            foreach ($items as $item) : ?>
                                <?php
                                $statement1 = $pdo->prepare("SELECT ITEM.NOME, LOCALIZACAO, TIPO.NOME FROM ITEM, TIPO WHERE TIPO.ID = IDTIPO and TIPO.NOME= :nome ;");
                                $statement1->bindValue(':nome', $item['NOME']);
                                $statement1->execute();
                                $tipo1 = $statement1->fetch(PDO::FETCH_ASSOC);
                                ?>
                                <tr>
                                    <td> <?php echo $item['NOME']; ?> </td>
                                    <td> <?php echo $item['LOCALIZACAO']; ?> </td>
                                    <td> <?php echo $tipo1['NOME'] ?> </td>
                                    <td class="buttons">
                                        <a class="editButton" href="updatenote.php?id=<?php echo $item['ID'] ?>" type="button">Editar</a>
                                        <form action="deletenote.php" method="POST">
                                            <input type="hidden" name="noteId" value="<?php echo $item['ID'] ?>">
                                            <button type="submit" class="deleteButton" type="button">Apagar</button>
                                        </form>
                                    </td>
                                </tr>
                            <?php endforeach;?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</body>
<?php include_once './partials/footer.php' ?>
