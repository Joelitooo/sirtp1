<div class="container">
    <div class="form-root">
        <div class="form-box-root">
            <div class="formbg-outer">
                <div class="edit-form-background">
                    <div class="edit-form-background-inner">
                        <span class="edit-form-title">Apontamento</span>
                        <form action="" method="POST">
                            <div class="boxes">
                                <label for="idtipo">Tipo</label>
                                <select name="idtipo">
                                    <?php
                                    $statement = $pdo->prepare("SELECT * FROM TIPO");
                                    $statement->execute();
                                    $tipos = $statement->fetchall(PDO::FETCH_ASSOC);

                                    foreach ($tipos as $tipo) :
                                        if ($item['IDTIPO'] == $tipo['ID']) { ?>
                                            <option value="<?php echo $tipo['ID'] ?>" selected><?php echo $tipo['NOME']; ?></option>
                                        <?php
                                        } else {
                                        ?>
                                            <option value="<?php echo $tipo['ID'] ?>"><?php echo $tipo['NOME']; ?></option>
                                        <?php
                                        }
                                        ?>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="boxes">
                                <label for="nome">Descrição</label>
                                <input type="text" name="nome" value="<?php echo $item['NOME'] ?>">
                            </div>
                            <div class="boxes">
                                <label for="localizacao">Localização</label>
                                <input type="text" name="localizacao" value="<?php echo $item['LOCALIZACAO'] ?? ' ' ?>">
                            </div>

                            <?php if(str_contains($_SERVER['REQUEST_URI'],'/updatenote.php')){ ?>
                            <div class="boxes">
                                <label for="ativo">Visível</label>
                                <select name="ativo" >
                                    <?php if($item['ATIVO'] == 1){ ?>
                                        <option value="1" selected>Sim</option>
                                        <option value="0">Não</option>
                                    <?php } else {?>
                                        <option value="1">Sim</option>
                                        <option value="0" selected>Não</option>
                                    <?php } ?>
                                </select>
                            </div>
                            <?php } ?>
                            <div class="boxes">
                                <input type="submit" name="submit" value="Guardar">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>