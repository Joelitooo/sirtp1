<?php
$host = 'host';
$dbname = 'dbname';
$user = 'user';
$password = 'password';
try {
    $pdo = new PDO("mysql:host=$host;dbname=$dbname;chaset=utf8", $user, $password);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (PDOException $e) {
    echo 'Erro durante a conexao à BD';
    echo $e->getMessage();
    exit();
}
?>