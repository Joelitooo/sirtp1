<?php include_once './dbOperations/createType.php'; ?>
<?php include_once './partials/header.php';?>

<body>
    <?php include_once './partials/navbar.php';?>
    <div class="container">
        <h1 class="title">Novo Tipo</h1>
        <?php include_once './partials/createTypeForm.php'?>
    </div>
</body>