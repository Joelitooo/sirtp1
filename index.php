<?php
require_once './dbconnect/connection.php';

$pesquisa = $_GET['pesquisa'] ?? null;

if ($pesquisa) {
    $statement = $pdo->prepare("SELECT * FROM ITEM WHERE NOME LIKE '%$pesquisa%'");
    $statement->execute();
    $items = $statement->fetchAll(PDO::FETCH_ASSOC);
} else {
    $statement = $pdo->prepare("SELECT * FROM ITEM");
    $statement->execute();
    $items = $statement->fetchAll(PDO::FETCH_ASSOC);
}


?>
<?php include_once "./partials/header.php"; ?>

<body>
    <?php include_once "./partials/navbar.php" ?>

    <div class="container">
        <h1 class="title">Apontamentos</h1>
        <a class="newNote" href="createnote.php">Criar novo</a>
        <form action="" class="searchForm">
            <div class="boxes">
                <input type="text" name="pesquisa" placeholder="Introduza a pesquisa">
                <button type="submit" type="button">Pesquisar</button>
            </div>
        </form>
        <div>
            <table class="notes-table">
                <thead>
                    <tr>
                        <th>Tipo</th>
                        <th>Descrição</th>
                        <th>Localização</th>
                        <th>Data / Hora</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if (sizeof($items) > 0) {
                        foreach ($items as $item) :
                            if ($item['ATIVO'] == 1) { ?>
                                <?php
                                $statement1 = $pdo->prepare("SELECT * FROM TIPO WHERE ID = :id");
                                $statement1->bindValue(':id', $item['IDTIPO']);
                                $statement1->execute();
                                $tipo = $statement1->fetch(PDO::FETCH_ASSOC);
                                ?>
                                <tr>
                                    <td> <?php echo $tipo['NOME']; ?> </td>
                                    <td> <?php echo $item['NOME']; ?> </td>
                                    <td> <?php echo $item['LOCALIZACAO']; ?> </td>
                                    <td> <?php echo $item['DATAHORA']; ?></td>
                                    <td class="buttons">
                                        <a class="editButton" href="updatenote.php?id=<?php echo $item['ID'] ?>" type="button">Editar</a>
                                        <form action="deletenote.php" method="POST">
                                            <input type="hidden" name="noteId" value="<?php echo $item['ID'] ?>">
                                            <button type="submit" class="deleteButton" type="button">Apagar</button>
                                        </form>
                                    </td>
                                </tr>
                        <?php }
                        endforeach;
                    } else { ?>
                        <td>Sem resultados correspondentes à pesquisa</td>
                    <?php } ?>

                </tbody>
            </table>
        </div>
    </div>

</body>
<?php include_once './partials/footer.php' ?>

