<?php include_once './dbOperations/createNote.php'; ?>
<?php include_once './partials/header.php';?>

<body>
    <?php include_once './partials/navbar.php';?>
    <div class="container">
        <h1 class="title">Novo Apontamento</h1>
        <?php include_once './partials/createUpdateForm.php'?>
    </div>
</body>