<?php
require_once './dbconnect/connection.php';

$id = $_GET['id'] ?? null;

if(!$_GET['id']) {
    header('location: tipos.php');
    exit;
}

$statement = $pdo->prepare("SELECT * FROM TIPO WHERE id = :id");
$statement->bindValue(':id', $id);
$statement->execute();
$item = $statement->fetch(PDO::FETCH_ASSOC);

$idTipo = $item['ID'];
$nome = $item['NOME'];

$errors = [];

if($_SERVER['REQUEST_METHOD'] === 'POST') {
    $nome = $_POST['nome'];

    if(!$nome) {
        $errors[]= 'A nome é obrigatório!';
    }

    if(empty($errors)) {

        $statement = $pdo->prepare("UPDATE TIPO set NOME = :nome WHERE ID = :id");    
        $statement->bindValue(':id',$id);
        $statement->bindValue(':nome', $nome);        
        $statement->execute();
        header('location: tipos.php');
    }
}
?>