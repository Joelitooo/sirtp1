<?php
require_once './dbconnect/connection.php';

$id = $_GET['id'] ?? null;

if(!$_GET['id']) {
    header('location: index.php');
    exit;
}

$statement = $pdo->prepare("SELECT * FROM ITEM WHERE id = :id");
$statement->bindValue(':id', $id);
$statement->execute();
$item = $statement->fetch(PDO::FETCH_ASSOC);

$nome = $item['NOME'];
$idTipo = $item['IDTIPO'];
$localizacaoItem = $item['LOCALIZACAO'];

$statement = $pdo->prepare("SELECT NOME FROM TIPO WHERE id = :id");
$statement->bindValue(':id', $idTipo);
$statement->execute();
$tipoItem = $statement->fetch(PDO::FETCH_ASSOC);

$errors = [];

if($_SERVER['REQUEST_METHOD'] === 'POST') {
    
    $nome = $_POST['nome'];
    $idTipo = $_POST['idtipo'];
    $localizacaoItem = $_POST['localizacao'];
    $ativo = (int)$_POST['ativo'];

    if(!$nome) {
        $errors[]= 'A nome é obrigatório!';
    }
    if(!$idTipo) {
        $errors[]= 'O idtipo é obrigatório!';
    }
    if(!$localizacaoItem) {
        $errors[]= 'A localizacao é obrigatório!';
    }

    if(empty($errors)) {
        $statement = $pdo->prepare("UPDATE ITEM set NOME = :nome, IDTIPO = :idtipo, LOCALIZACAO = :localizacao, ATIVO = :ativo WHERE ID = :id");
        var_dump(is_int($ativo));

        $statement->bindValue(':id', $id);
        $statement->bindValue(':nome', $nome);
        $statement->bindValue(':idtipo', $idTipo);
        $statement->bindValue(':localizacao', $localizacaoItem);
        $statement->bindValue(':ativo', $ativo);
        
        $statement->execute();
        header('location: index.php');
    }
}
?>