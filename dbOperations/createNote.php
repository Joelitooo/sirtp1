<?php
require_once './dbconnect/connection.php';

$errors = [];
$nome = ' ';
$idTipo = ' ';
$localizacaoItem = ' ';

if($_SERVER['REQUEST_METHOD'] === 'POST') {
    $nome = $_POST['nome'];
    $idTipo = $_POST['idtipo'];
    $localizacaoItem = $_POST['localizacao'];

    if(!$nome) {
        $errors[]= 'O nome é obrigatório!';
    }
    if(!$idTipo) {
        $errors[]= 'O tipo é obrigatório!';
    }
    if(!$localizacaoItem) {
        $errors[]= 'A localizacao é obrigatória!';
    }

    if(empty($errors)) {
        $date = new DateTime();
        $date = $date->format('Y-m-d H:i:s');
        $statement = $pdo->prepare("INSERT INTO ITEM(NOME, IDTIPO, LOCALIZACAO, DATAHORA, ativo) VALUES (:nome, :idtipo, :localizacao, :datahora, :ativo)");
        $statement->bindValue(':nome', $nome);
        $statement->bindValue(':idtipo', $idTipo);
        $statement->bindValue(':localizacao', $localizacaoItem);
        $statement->bindValue(':datahora', $date);
        $statement->bindValue(':ativo', TRUE);

        $statement->execute();
        header('location: index.php');
    }
}
?>

