<?php
require_once './dbconnect/connection.php';

$id = $_POST['typeId'] ?? null;

if(!$_POST['typeId']) {
    header('location: tipos.php');
    exit;
}

$statement = $pdo->prepare("DELETE FROM TIPO WHERE ID = :id");
$statement->bindValue(':id',$id);
$statement->execute();

header('location: tipos.php');
?>