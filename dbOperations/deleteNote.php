<?php
require_once './dbconnect/connection.php';

$id = $_POST['noteId'] ?? null;

if(!$_POST['noteId']) {
    header('location: index.php');
    exit;
}

$statement = $pdo->prepare("DELETE FROM ITEM WHERE ID = :id");
$statement->bindValue(':id',$id);
$statement->execute();

header('location: index.php');
?>