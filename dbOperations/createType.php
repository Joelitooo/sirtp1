<?php
require_once './dbconnect/connection.php';

$errors = [];
$idTipo = '';
$nome = '';

if($_SERVER['REQUEST_METHOD'] === 'POST') {
    $nome = $_POST['nome'];    
    
    if(!$nome) {
        $errors[]= 'O nome é obrigatório!';
    }
    

    if(empty($errors)) {

        $statement = $pdo->prepare("INSERT INTO TIPO(NOME) VALUES (:nome)");
        $statement->bindValue(':nome', $nome);       
        $statement->execute();
        header('location: tipos.php');
    }
}
?>

